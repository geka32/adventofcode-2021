from collections import defaultdict


tmp_board = defaultdict(int)
board = []
instructions = []

with open('13_01.data') as file:
    while line := file.readline().strip():
        x, y = map(int, line.split(','))
        tmp_board[y] |= (1 << x)
    for _ in range(max(tmp_board) + 1):
        board.append(tmp_board[_])
    while line := file.readline().strip():
        along, num = line.split('=')
        instructions.append((along[-1], int(num)))

for along, num in instructions[:1]:
    if along == 'y':
        for idx in range(num+1, len(board)):
            board[len(board) - idx - 1] |= board[idx]
        board = board[:num]
    else:
        for idx, row in enumerate(board):
            tmp = row >> (num + 1)
            for idx2 in range(num-1, -1, -1):
                mask = 1 << idx2
                bit = (row & mask) >> idx2
                tmp |= bit << (num - 1 - idx2)
            board[idx] = tmp

ans = 0
for row in board:
    while row:
        ans += row & 1
        row >>= 1
print(ans)

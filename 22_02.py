from bitarray import bitarray


steps = []
with open('22_02.data') as file:
    while line := file.readline().strip():
        on_off, ranges = line.split()
        x, y, z = line.split(',')
        x1, x2 = (int(_) for _ in x.split('=')[1].split('..'))
        y1, y2 = (int(_) for _ in y.split('=')[1].split('..'))
        z1, z2 = (int(_) for _ in z.split('=')[1].split('..'))
        steps.append((on_off == "on", ((x1, x2+1), (y1, y2+1), (z1, z2+1))))

x_slices, y_slices, z_slices = set(), set(), set()
for _, coords in steps:
    x_slices.update(coords[0])
    y_slices.update(coords[1])
    z_slices.update(coords[2])
x_slices = sorted(x_slices)
y_slices = sorted(y_slices)
z_slices = sorted(z_slices)

x_map = {val: idx for idx, val in enumerate(x_slices)}
y_map = {val: idx for idx, val in enumerate(y_slices)}
z_map = {val: idx for idx, val in enumerate(z_slices)}

reactor = bitarray(len(x_slices) * len(y_slices) * len(z_slices))
reactor.setall(False)

for on_off, coords in steps:
    z_line_len = len(z_slices)
    yz_square_len = len(y_slices) * z_line_len
    cube_line_len = z_map[coords[2][1]] - z_map[coords[2][0]]

    offset_x = x_map[coords[0][0]] * yz_square_len
    for x in range(x_map[coords[0][0]], x_map[coords[0][1]]):
        offset_y = offset_x + y_map[coords[1][0]] * z_line_len
        for y in range(y_map[coords[1][0]], y_map[coords[1][1]]):
            offset_z = offset_y + z_map[coords[2][0]]
            reactor[offset_z:offset_z + cube_line_len] = on_off
            offset_y += z_line_len
        offset_x += yz_square_len

total_on = 0
offset = 0
for x in range(len(x_slices)-1):
    for y in range(len(y_slices)-1):
        for z in range(len(z_slices)-1):
            if reactor[offset]:
                total_on += (x_slices[x+1] - x_slices[x]) * \
                    (y_slices[y+1] - y_slices[y]) * \
                    (z_slices[z+1] - z_slices[z])
            offset += 1
        offset += 1
    offset += len(z_slices)

print(total_on)

with open('04_01.data') as file:
    numbers = [int(_) for _ in next(file).split(',')]
    cards = []
    try:
        while True:
            card_raw = [next(file) for _ in range(6)]
            cards.append(
                {'raw': [[int(_) for _ in card_line.split()] for card_line in card_raw[1:]]})
    except StopIteration:
        pass

for card in cards:
    card['rows'] = [{_ for _ in row} for row in card['raw']]
    card['rows_unmarked'] = [5] * 5
    card['cols'] = [{_ for _ in col} for col in [[row[idx] for row in card['raw']]for idx in range(5)]]
    card['cols_unmarked'] = [5] * 5
    card['sum'] = sum(sum(_) for _ in card['raw'])

try:
    for num in numbers:
        for card in cards:
            for idx, row in enumerate(card['rows']):
                if num in row:
                    card['sum'] -= num
                    card['rows_unmarked'][idx] -= 1
                if not card['rows_unmarked'][idx]:
                    raise StopIteration
            for idx, col in enumerate(card['cols']):
                if num in col:
                    card['cols_unmarked'][idx] -= 1
                if not card['cols_unmarked'][idx]:
                    raise StopIteration
except StopIteration:
    print(num * card['sum'])

image = []
with open('20_02.data') as file:
    algorithm = [0 if _ == '.' else 1 for _ in file.readline().strip()]
    for line in file.readlines()[1:]:
        image.append([0, 0] + [0 if _ == '.' else 1 for _ in line.strip()] +
                     [0, 0])
    image.insert(0, [0] * len(image[0]))
    image.insert(0, [0] * len(image[0]))
    image.append([0] * len(image[0]))
    image.append([0] * len(image[0]))

padding = 0
for _ in range(50):
    if image[0][0] == 0 and algorithm[0] == 1:
        padding = 1
    elif image[0][0] == 1 and algorithm[-1] == 0:
        padding = 0
    ans = 0
    next_image = [([padding] * (len(image[0])+2)) for row in range(2)]
    for row in range(1, len(image)-1):
        next_image.append([padding] * 2)
        for col in range(1, len(image[0])-1):
            code = 0
            for idx, r_c in enumerate((r, c) for r in range(row-1, row+2) for c in range(col-1, col+2)):
                code |= image[r_c[0]][r_c[1]] << (8 - idx)
            light = algorithm[code]
            ans += light
            next_image[-1].append(light)
        next_image[-1].extend([padding] * 2)
    next_image.extend(([padding] * (len(image[0])+2)) for row in range(2))
    image = next_image

print(ans)

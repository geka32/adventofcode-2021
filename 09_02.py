board = []
with open('09_02.data') as file:
    for line in file.readlines():

        row = [9] + [int(_) for _ in line.strip()] + [9]
        board.append(row)
board.insert(0, [9] * len(board[0]))
board.append([9] * len(board[0]))


def dfs(col: int, row: int) -> int:
    if board[row][col] == 9:
        return 0
    board[row][col] = 9
    return 1 + dfs(col+1, row) + dfs(col-1, row) + dfs(col, row+1) + dfs(col, row-1)


basins = []
for col, row in [(col, row) for row in range(1, len(board) - 1) for col in range(1, len(board[0]) - 1)]:
    basin = dfs(col, row)
    if basin > 0:
        basins.append(basin)

basins.sort(reverse=True)
print(basins[0] * basins[1] * basins[2])

steps = []
with open('22_01.data') as file:
    while line := file.readline().strip():
        on_off, ranges = line.split()
        x, y, z = line.split(',')
        x1, x2 = (int(_) for _ in x.split('=')[1].split('..'))
        y1, y2 = (int(_) for _ in y.split('=')[1].split('..'))
        z1, z2 = (int(_) for _ in z.split('=')[1].split('..'))
        steps.append((1 if on_off == "on" else 0, ((x1, x2), (y1, y2), (z1, z2))))

max_side = 50
max_range = max_side * 2 + 1
reactor = [[[0] * (max_range) for y in range(max_range)] for x in range(max_range)]

total_on = 0
for on_off, ranges in steps:
    x1, x2 = ranges[0][0] + max_range // 2, ranges[0][1] + max_range // 2
    if x1 > max_range or x2 < 0:
        continue
    for x in range(max(x1, 0), min(x2 + 1, max_range + 1)):
        y1, y2 = ranges[1][0] + max_range // 2, ranges[1][1] + max_range // 2
        if y1 > max_range or y2 < 0:
            continue
        for y in range(max(y1, 0), min(y2 + 1, max_range + 1)):
            z1, z2 = ranges[2][0] + max_range // 2, ranges[2][1] + max_range // 2
            if z1 > max_range or z2 < 0:
                continue
            for z in range(max(z1, 0), min(z2 + 1, max_range + 1)):
                if reactor[x][y][z] and not on_off:
                    total_on -= 1
                elif not reactor[x][y][z] and on_off:
                    total_on += 1
                reactor[x][y][z] = on_off

print(total_on)

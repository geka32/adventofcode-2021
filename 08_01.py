patterns, digits = [], []
with open('08_01.data') as file:
    for line in file.readlines():
        p, d = line.split('|')
        patterns.append(p.split())
        digits.append(d.split())

# for _ in patterns: print(_)
# for _ in digits: print(_)

ans = sum(len([_ for _ in d if len(_) in {2, 4, 3, 7}]) for d in digits)
print(ans)
with open('25_01.data') as file:
    # 0 - empty
    # 1 - moving east
    # 2 - moving south
    m = {'.': 0, '>': 1, 'v': 2}
    board = [[m[c] for c in _.strip()] for _ in file]

board_rows = len(board)
board_cols = len(board[0])

moves_done = 0
while True:
    moves_done += 1
    move_done = False
    next_board = [[0] * board_cols for _ in range(board_rows)]

    # move east
    unmoved = []
    for row in range(board_rows):
        for col in range(board_cols):
            if board[row][col] == 1:
                next_col = (col + 1) % board_cols
                if not board[row][next_col]:
                    next_board[row][next_col] = 1
                    move_done = True
                else:
                    unmoved.append((row, col))
    # populate unmoved
    for row, col in unmoved:
        next_board[row][col] = 1

    # move south
    unmoved = []
    for row in range(board_rows):
        for col in range(board_cols):
            if board[row][col] == 2:
                next_row = (row + 1) % board_rows
                if board[next_row][col] != 2 and not next_board[next_row][col]:
                    next_board[next_row][col] = 2
                    move_done = True
                else:
                    unmoved.append((row, col))
    # populate unmoved
    for row, col in unmoved:
        next_board[row][col] = 2

    if not move_done:
        break

    board = next_board

print(moves_done)
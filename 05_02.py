lines, max_x, max_y = [], 0, 0
with open('05_02.data') as file:
    for line in file:
        data = [_ for _ in line.split()]
        p1 = tuple(map(int, data[0].split(',')))
        p2 = tuple(map(int, data[2].split(',')))
        max_x = max(max_x, p1[0], p2[0])
        max_y = max(max_y, p1[1], p2[1])
        lines.append((p1, p2))

board = [[0 for _ in range(max_x + 1)] for _ in range(max_y + 1)]

for p, q in lines:
    dx = q[0] - p[0]
    dy = q[1] - p[1]
    if dx and dy:
        if dx < 0:
            p, q = q, p
        y = p[1]
        dy = 1 if q[1] - p[1] > 0 else -1
        for x in range(p[0], q[0] + 1):
            board[y][x] += 1
            y += dy
    elif dx:
        y = q[1]
        if dx < 0:
            p, q = q, p
        for x in range(p[0], q[0] + 1):
            board[y][x] += 1
    else:
        x = q[0]
        if dy < 0:
            p, q = q, p
        for y in range(p[1], q[1] + 1):
            board[y][x] += 1

# for row in board: print(row)

print(sum(sum(_ > 1 for _ in row) for row in board))
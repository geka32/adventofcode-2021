positions = []
with open('07_02.data') as file:
    for num in file.readline().split(','):
        positions.append(int(num))


positions.sort()

#  ans = float('inf')
#  for target in range(positions[-1] + 1):
#      ans = min(ans, int(sum(
#          (1 + abs(target - position)) / 2 * abs(target - position)
#          for position in positions
#      )))
#
#  print(ans)

def need_fuel(target):
    return int(sum(
        (1 + abs(target - position)) / 2 * abs(target - position)
        for position in positions
    ))

lo, hi = positions[0], positions[-1]
while lo <= hi:
    lo_fuel, hi_fuel = need_fuel(lo), need_fuel(hi)
    mid = (hi - lo) // 2 + lo
    mid_fuel = need_fuel(mid)
    #  print(f'{lo=}, {mid=}, {hi=}')
    #  print(f'{lo_fuel=}, {mid_fuel=}, {hi_fuel=}')
    if lo_fuel == hi_fuel:
        lo_fuel = mid_fuel
        break
    elif lo_fuel < hi_fuel:
        hi = mid
    else:
        lo = mid + 1

print(lo_fuel)

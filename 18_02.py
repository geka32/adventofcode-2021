from __future__ import annotations
from dataclasses import dataclass, field
from copy import deepcopy
from itertools import permutations


@dataclass
class Node:
    parent: Node | None = field(repr=False)
    val: int | None = field(default=None)
    left: Node | None = field(default=None)
    right: Node | None = field(default=None)


def parse_num(num: list | int, root: Node):
    if isinstance(num, list):
        root.left = Node(root)
        parse_num(num[0], root.left)
        root.right = Node(root)
        parse_num(num[1], root.right)
    else:   # digit
        root.val = num


nums = []
with open('18_02.data') as file:
    while line := file.readline().strip():
        node = Node(None)
        parse_num(eval(line), node)
        nums.append(node)


def repr_node(node: Node) -> str:
    if node.val is None:
        return f'[{repr_node(node.left)},{repr_node(node.right)}]'
    return str(node.val)


def increase_left(node: Node, visited: int, val: int) -> bool:
    if not node:
        return False
    elif node.val is not None:
        node.val += val
        return True
    elif id(node.left) == visited:
        return increase_left(node.parent, id(node), val)
    elif id(node.right) == visited:
        return increase_left(node.left, 0, val)
    else:
        return increase_left(node.right, 0, val) \
            or increase_left(node.left, 0, val)


def increase_right(node: Node, visited: int, val: int) -> bool:
    if not node:
        return False
    elif node.val is not None:
        node.val += val
        return True
    elif id(node.right) == visited:
        return increase_right(node.parent, id(node), val)
    elif id(node.left) == visited:
        return increase_right(node.right, 0, val)
    else:
        return increase_right(node.left, 0, val) \
            or increase_right(node.right, 0, val)


def explode(node: Node, level: int) -> bool:
    if node.val is None:
        if level < 4:
            return explode(node.left, level+1) or explode(node.right, level+1)
        increase_left(node.parent, id(node), node.left.val)
        increase_right(node.parent, id(node), node.right.val)
        node.val, node.left, node.right = 0, None, None
        return True
    return False


def split(node: Node) -> bool:
    if node.val is not None:
        if node.val < 10:
            return False
        node.left = Node(node, node.val // 2)
        node.right = Node(node, (node.val + 1) // 2)
        node.val = None
        return True
    return split(node.left) or split(node.right)


def reduce(node: Node, level: int) -> bool:
    return explode(node, level) or split(node)


def add(a: Node, b: Node) -> Node:
    c = Node(None, None, a, b)
    a.parent, b.parent = c, c
    return c


def magnitude(node: Node) -> int:
    if node.val is not None:
        return node.val
    return 3 * magnitude(node.left) + 2 * magnitude(node.right)


ans = 0
for i, j in permutations(range(len(nums)), 2):
    a, b = deepcopy(nums[i]), deepcopy(nums[j])
    c = add(a, b)
    while reduce(c, 0):
        pass
    ans = max(ans, magnitude(c))

print(ans)

school = [0] * 9
with open('06_01.data') as file:
    for num in file.readline().split(','):
        school[int(num)] += 1

for _ in range(80):
    ready = school.pop(0)
    school[6] += ready
    school.append(ready)

print(sum(school))

from collections import Counter


rules = {}
with open('14_01.data') as file:
    template = file.readline().strip()
    for line in [_.strip() for _ in file.readlines() if _.strip()]:
        pair, el = line.split(' -> ')
        rules[pair] = el

for _ in range(10):
    tmp = ""
    for idx in range(len(template) - 1):
        tmp += template[idx] + rules[template[idx:idx+2]]
    tmp += template[idx+1]
    template = tmp

cnt = Counter(template)
print(cnt.most_common(1)[0][1] - cnt.most_common()[-1][1])

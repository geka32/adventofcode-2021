from typing import List


patterns, digits = [], []
with open('08_02.data') as file:
    for line in file.readlines():
        p, d = line.split('|')
        patterns.append({idx: set(patt) for idx, patt in enumerate(
            sorted(list(map(sorted, p.split())), key=len), 1)})
        digits.append(list(map(sorted, d.split())))

# for pattern in patterns:
#    print(pattern)
# for digit in digits:
#     print([''.join(_) for _ in digit])


def decode_segments(p: set) -> List[str]:
    d1 = p[1]
    d7 = p[2]
    d4 = p[3]
    d8 = p[10]
    d235 = p[4], p[5], p[6]
    d690 = p[7], p[8], p[9]

    cf = d1
    # print(f'{cf=}')

    a = d7 - cf
    # print(f'{a=}')

    bd = d4 - cf
    # print(f'{bd=}')

    eg = d8 - a - cf - bd
    # print(f'{eg=}')

    for digit in d690:
        if c := cf - digit:
            d6 = digit
            # print(f'{c=}')
            # print(f'{d6=}')
            break

    f = cf - c
    # print(f'{f=}')

    for digit in d235:
        if len(d6 - digit) == 1:
            e = d6 - digit
            d5 = digit
            # print(f'{e=}')
            # print(f'{d5=}')

    g = eg - e
    # print(f'{g=}')

    d23 = [_ for _ in d235 if _ != d5]
    # print(f'{d23=}')

    for digit in d23:
        if not f & digit:
            d = digit - a - c - eg
            # print(f'{d=}')
            break

    b = bd - d
    # print(f'{b=}')

    return [*a, *b, *c, *d, *e, *f, *g]


def decode_digit(digit: List[str], segments: List[str]) -> int:
    dlen = len(digit)
    if dlen == 2:
        return 1
    elif dlen == 4:
        return 4
    elif dlen == 3:
        return 7
    elif dlen == 7:
        return 8
    elif dlen == 5:
        if segments[1] in digit:
            return 5
        elif segments[4] in digit:
            return 2
        else:
            return 3
    elif segments[3] not in digit:
        return 0
    elif segments[4] in digit:
        return 6
    else:
        return 9


ans = 0
for idx, p in enumerate(patterns):
    segments = decode_segments(p)
    num = 0
    for digit in digits[idx]:
        num = num * 10 + decode_digit(digit, segments)
    ans += num

print(ans)

with open('17_02.data') as file:
    line = file.readline().strip()
    _, line = line.split(':')
    x, y = line.split(',')
    x1, x2 = (int(_) for _ in x.split('=')[1].split('..'))
    y2, y1 = (int(_) for _ in y.split('=')[1].split('..'))


def get_max_y(dx, dy):
    x, y = 0, 0
    while x <= x2 and y >= y2:
        x += dx
        dx = dx - 1 if dx else dx
        y += dy
        dy -= 1
        if x < x1 or y > y1:    # not reached the target
            continue
        return x <= x2 and y >= y2


dxes = []
for dx in range(x2, 1, -1):
    p, steps = 0, 0
    orig_dx = dx
    while p <= x2 and dx:
        p += dx
        steps += 1
        dx -= 1
        if x1 <= p <= x2:
            dxes.append(orig_dx)
            break
        elif p > x2:
            break

min_dy = y2
max_dy = 1 - y2

ans = 0
for dx in dxes:
    for dy in range(min_dy, max_dy):
        if get_max_y(dx, dy):
            ans += 1

print(ans)

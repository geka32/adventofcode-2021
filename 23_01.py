from __future__ import annotations
from dataclasses import dataclass, field
from heapq import heappush, heappop


board = [None] * 15
with open('23_01.data') as file:
    lines = file.readlines()
    board[0] = lines[3][3]
    board[1] = lines[2][3]
    board[2] = lines[3][5]
    board[3] = lines[2][5]
    board[4] = lines[3][7]
    board[5] = lines[2][7]
    board[6] = lines[3][9]
    board[7] = lines[2][9]

# board:  8  9    10    11   12    13  14
#              1     3     5    7
#              0     2     4    6
# graph: from -> [(to, weight), ...]


def print_board(board: list) -> None:
    board = [*map(lambda el: el or '.', board[:])]
    print(board[8], ' . '.join(board[9:14]), board[14])
    print('   ', '   '.join(board[1:8:2]))
    print('   ', '   '.join(board[0:7:2]))


def merge_graph(a: dict, b: dict) -> dict:
    'Merge all items from b into a'
    for k, v in b.items():
        if k in a:
            a[k].extend(v)
        else:
            a[k] = v


any_graph = {
    0: [(1, 1)],
    1: [(9, 2), (10, 2)],
    2: [(3, 1)],
    3: [(10, 2), (11, 2)],
    4: [(5, 1)],
    5: [(11, 2), (12, 2)],
    6: [(7, 1)],
    7: [(12, 2), (13, 2)],

    8: [(9, 1)],
    9: [(8, 1), (10, 2)],
    10: [(9, 2), (11, 2)],
    11: [(10, 2), (12, 2)],
    12: [(11, 2), (13, 2)],
    13: [(12, 2), (14, 1)],
    14: [(13, 1)],
}
a_graph = {
    9: [(1, 2)],
    10: [(1, 2)],
    1: [(0, 1)]
}
merge_graph(a_graph, any_graph)
b_graph = {
    10: [(3, 2)],
    11: [(3, 2)],
    3: [(2, 1)]
}
merge_graph(b_graph, any_graph)
c_graph = {
    11: [(5, 2)],
    12: [(5, 2)],
    5: [(4, 1)]
}
merge_graph(c_graph, any_graph)
d_graph = {
    12: [(7, 2)],
    13: [(7, 2)],
    7: [(6, 1)]
}
merge_graph(d_graph, any_graph)


class Amphipod:
    def __init__(self, id: int, type: str, position: int, graph: dict, home: tuple, k: int) -> None:
        self.id = id
        self.type = type
        self.position = position
        self.graph = graph
        self.home = home
        self.k = k
        self.visited = set()
        self.moves_done = 0

    def __repr__(self) -> str:
        return f'<id: {self.id}, position: {self.position}, moves_done={self.moves_done}>'

    def at_home(self, board: list) -> bool:
        """
        Check if amphipod is at home
        """
        return self.home[0] <= self.position <= self.home[-1] and all(
            self.position == home or board[home] == self.type
            for home in self.home if home <= self.position
        )

    def home_available(self, board: list) -> bool:
        """
        Check if home is available for amphipod
        """
        prev_home_empty = False
        for home in self.home:
            if prev_home_empty and board[home] or board[home] and board[home] != self.type:
                return False
            prev_home_empty = not board[home]
        return True

    def in_hall(self) -> bool:
        """
        Check if amphipod in hall
        """
        return self.position >= 8

    def can_move(self, board: list) -> bool:
        """
        Check if amphipod can take a move
        """
        if self.moves_done > 1:
            return False
        if self.at_home(board):
            return False
        if not self.moves_done:
            return True
        if self.in_hall() and self.home_available(board):
            return True
        return not self.in_hall()

    def can_pass_move(self, board: list) -> bool:
        """
        Check if amphipod can pass move
        """
        return self.at_home(board) or \
            self.moves_done < 2 and self.in_hall()


pos = board.index("A")
a0 = Amphipod(0, "A", pos, a_graph, (0, 1), 1)
pos = board.index("A", pos+1)
a1 = Amphipod(1, "A", pos, a_graph, (0, 1), 1)

pos = board.index("B")
b0 = Amphipod(2, "B", pos, b_graph, (2, 3), 10)
pos = board.index("B", pos+1)
b1 = Amphipod(3, "B", pos, b_graph, (2, 3), 10)

pos = board.index("C")
c0 = Amphipod(4, "C", pos, c_graph, (4, 5), 100)
pos = board.index("C", pos+1)
c1 = Amphipod(5, "C", pos, c_graph, (4, 5), 100)

pos = board.index("D")
d0 = Amphipod(6, "D", pos, d_graph, (6, 7), 1000)
pos = board.index("D", pos+1)
d1 = Amphipod(7, "D", pos, d_graph, (6, 7), 1000)


def check_end(board: list) -> bool:
    """
    Check if game is over
    """
    return board[0] == board[1] == 'A' and board[2] == board[3] == 'B' and \
        board[4] == board[5] == 'C' and board[6] == board[7] == 'D'


@dataclass
class PlayerState:
    position: int = field()
    visited: set = field()
    moves_done: int = field


@dataclass(order=True)
class GameState:
    energy: int = field()
    board: list = field(compare=False)
    players: list[PlayerState] = field(compare=False)
    curr_player_id: int = field()
    prev_player_id: int | None = field(default=None, compare=False)

    @staticmethod
    def dump(energy: int, board: list, players: list[Amphipod], curr_player: Amphipod,
             prev_player: Amphipod | None) -> GameState:
        """
            Dump game state
        """
        player_states = [
            PlayerState(player.position, player.visited.copy(),
                        player.moves_done)
            for player in players
        ]

        return GameState(energy, board, player_states, curr_player.id,
                         prev_player.id if prev_player else None)

    @staticmethod
    def restore(game: GameState, players: list[Amphipod]) -> \
            tuple[int, list, list[Amphipod], Amphipod, Amphipod | None]:
        """
            Restore game state
        """
        for player, state in zip(players, game.players):
            player.position = state.position
            player.visited = state.visited
            player.moves_done = state.moves_done
        return game.energy, game.board, players, players[game.curr_player_id], \
            players[game.prev_player_id] if game.prev_player_id is not None else None


def get_next_move(board: list, energy: int, seen_boards: set, players: list[Amphipod],
                  player: Amphipod, prev_player: Amphipod | None = None) -> GameState:
    """
        Generate next move for player.
    """
    if not player.can_move(board):
        return

    for next_position, weight in player.graph[player.position]:
        if board[next_position] or next_position in player.visited:
            # print(f' {next_position=} is {"ocuppied" if board[next_position] else "visited"}. skip')
            continue
        new_board = board.copy()
        new_board[player.position], new_board[next_position] = new_board[next_position], new_board[player.position]
        new_board_tuple = tuple((player.type, next_position, *new_board))
        if new_board_tuple in seen_boards:
            continue
        seen_boards.add(new_board_tuple)
        curr_position, player.position = player.position, next_position
        player.visited.add(next_position)
        yield GameState.dump(energy + weight * player.k, new_board, players, player, prev_player)
        player.visited.remove(next_position)
        player.position = curr_position


def play_game(players: list[Amphipod], board: list) -> int:
    """
        Play game and return min energy to finish it.
    """

    seen_boards = set()
    q = []
    for player in players:
        player.visited.add(player.position)
        for move in get_next_move(board, 0, seen_boards, players, player):
            heappush(q, move)
        player.visited.remove(player.position)

    while q:
        game: GameState = heappop(q)
        energy, board, players, curr_player, prev_player = GameState.restore(
            game, players)

        if check_end(board):
            return energy

        # take own move
        move_taken = False
        for move in get_next_move(board, energy, seen_boards, players, curr_player, curr_player):
            # print('**** continue own move')
            # print_board(board)
            # print_board(move.board)
            heappush(q, move)
            move_taken = True

        # do not pass move if player has not taken a move
        if not move_taken and (not prev_player or prev_player.id != curr_player.id):
            continue

        # pass move to another player
        if curr_player.can_pass_move(board):
            curr_player.moves_done += 1
            curr_player.visited = set()
            for player in [_ for _ in players if _.id != curr_player.id]:
                player.visited.add(player.position)
                for move in get_next_move(board, energy, seen_boards, players, player, curr_player):
                    # print('**** pass move move')
                    # print_board(board)
                    # print_board(move.board)
                    heappush(q, move)
                player.visited.remove(player.position)

    return float('inf')


players = [a0, a1, b0, b1, c0, c1, d0, d1]
min_energy = play_game(players, board)
print(min_energy)

input_len = 12
gamma = [0] * input_len
total_records = 0
masks = [1 << _ for _ in range(input_len - 1, -1, -1)]

with open('03_01.data') as file:
    for diag in file:
        total_records += 1
        diag = int(diag, 2)
        for idx in range(input_len):
            if diag & masks[idx]:
                gamma[idx] += 1

gamma_rate, epsilon_rate = 0, 0
for idx in range(input_len):
    gamma_rate |= masks[idx] if gamma[idx] > total_records // 2 else 0
    epsilon_rate = ((1 << input_len) - 1) ^ gamma_rate

print(gamma_rate * epsilon_rate)

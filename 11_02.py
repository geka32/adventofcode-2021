board = []
with open('11_02.data') as file:
    for line in file.readlines():
        row = [int(_) for _ in line.strip()]
        board.append(row)
# for _ in board:
#     print(_)


def start_round():
    flashes_count = 0
    for col, row in [(col, row) for row in range(10) for col in range(10)]:
        board[row][col] += 1
        if board[row][col] == 10:
            flashes[row][col] += 1
            flashes_count += 1
    return flashes_count


def charge_neighbors(row, col, next_flashes):
    flashes_count = 0
    for c, r in [(c, r) for r in range(row-1, row+2)
                 for c in range(col-1, col+2)
                 if 0 <= c < 10 and 0 <= r < 10 and (c, r) != (col, row)]:
        board[r][c] += 1
        if board[r][c] == 10:
            next_flashes[r][c] += 1
            flashes_count += 1
    return flashes_count


def reset_flashed():
    for col, row in [(col, row) for row in range(10) for col in range(10)]:
        if board[row][col] >= 10:
            board[row][col] = 0


ans = 0
flashes = [[0] * 10 for _ in range(10)]

while True:
    ans += 1
    flashes_in_round = start_round()

    flashes_count = flashes_in_round
    while flashes_count:
        flashes_count = 0
        next_flashes = [[0] * 10 for _ in range(10)]
        for col, row in [(col, row) for row in range(10) for col in range(10)]:
            if flashes[row][col]:
                flashes_count += charge_neighbors(row, col, next_flashes)
        flashes = next_flashes
        flashes_in_round += flashes_count

    if flashes_in_round == 100:
        break

    reset_flashed()

print(ans)

positions = []
with open('21_01.data') as file:
    while line := file.readline().strip():
        _, position = line.split(':')
        positions.append(int(position))


class Counter:
    def __init__(self):
        self._value = 0

    def increase(self):
        self._value += 1

    @property
    def value(self):
        return self._value


def roll_dice(counter: Counter) -> int:
    """
    Generator to roll a dice
    """
    value = 1
    while True:
        counter.increase()
        yield value
        value = (value + 1) % 100 or 100


def get_player(number_of_players: int) -> int:
    """
    Generator to get next player
    """
    player = 0
    while True:
        yield player
        player = (player + 1) % number_of_players


scores = [0] * len(positions)
counter = Counter()
roller = roll_dice(counter)
players = get_player(2)

while True:
    player = next(players)
    positions[player] = (positions[player] + sum((next(roller) for _ in range(3)))) % 10 or 10
    scores[player] += positions[player]
    if scores[player] >= 1000:
        break

print(scores[next(players)] * counter.value)

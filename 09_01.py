board = []
with open('09_01.data') as file:
    for line in file.readlines():

        row = [float('inf')] + [int(_) for _ in line.strip()] + [float('inf')]
        board.append(row)
board.insert(0, [float('inf')] * len(board[0]))
board.append([float('inf')] * len(board[0]))

ans = 0
for col, row in [(col, row) for row in range(1, len(board) - 1) for col in range(1, len(board[0]) - 1)]:
    height = board[row][col]
    if height < board[row-1][col] and height < board[row+1][col] \
            and height < board[row][col-1] and height < board[row][col+1]:
        ans += 1 + height

print(ans)

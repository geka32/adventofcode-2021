from collections import Counter


positions = []
with open('21_02.data') as file:
    while line := file.readline().strip():
        _, position = line.split(':')
        positions.append(int(position))


def roll_dice() -> int:
    """
    Roll dice 3 times and return the sum of scores
    """
    return (sum((a, b, c)) for a in range(1, 4) for b in range(1, 4) for c in range(1, 4))


def dfs(player: int, scores_0: int, scores_1: int, pos_0: int, pos_1: int,
        dice_sum: int, mult: int) -> None:
    global wins_0, wins_1

    if player:
        pos_1 = (pos_1 + dice_sum) % 10 or 10
        scores_1 += pos_1
        if scores_1 >= 21:
            wins_1 += mult
            return
    else:
        pos_0 = (pos_0 + dice_sum) % 10 or 10
        scores_0 += pos_0
        if scores_0 >= 21:
            wins_0 += mult
            return
    for ds, m in all_dice_sums:
        dfs(0 if player else 1, scores_0, scores_1, pos_0, pos_1, ds, m * mult)


wins_0 = 0
wins_1 = 0
all_dice_sums = [_ for _ in Counter(roll_dice()).items()]
dfs(1, 0, -positions[1], positions[0], positions[1], 0, 1)
print(wins_0 if wins_0 > wins_1 else wins_1)

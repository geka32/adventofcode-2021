from collections import Counter, defaultdict


rules = {}
pairs = defaultdict(int)
with open('14_02.data') as file:
    template = file.readline().strip()
    cnt = Counter(template)
    for idx in range(len(template) - 1):
        pairs[template[idx:idx+2]] += 1
    for line in [_.strip() for _ in file.readlines() if _.strip()]:
        pair, el = line.split(' -> ')
        rules[pair] = el

for _ in range(40):
    tmp = defaultdict(int)
    for pair in pairs:
        tmp[pair[0]+rules[pair]] += pairs[pair]
        tmp[rules[pair]+pair[1]] += pairs[pair]
        cnt[rules[pair]] += pairs[pair]
    pairs = tmp

print(cnt.most_common(1)[0][1] - cnt.most_common()[-1][1])

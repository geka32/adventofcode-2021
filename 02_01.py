h, d = 0, 0
with open('02_01.data') as file:
    for cmd in file:
        direction, steps = cmd.split()
        if direction.startswith('f'):
            h += int(steps)
        elif direction.startswith('d'):
            d += int(steps)
        else:
            d -= int(steps)
print(h * d)

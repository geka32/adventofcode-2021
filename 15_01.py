board = []
with open('15_01.data') as file:
    while line := file.readline().strip():
        board.append(line)

dp = [[float('inf')] * (len(board[0]) + 1) for _ in range(len(board) + 1)]
dp[0][1] = 0
for row in range(1, len(dp)):
    for col in range(1, len(dp[0])):
        dp[row][col] = min(int(board[row-1][col-1]) + dp[row-1][col],
                           int(board[row-1][col-1]) + dp[row][col-1])
print(dp[-1][-1] - dp[1][1])

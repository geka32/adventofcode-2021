prev, ans = 0, 0
with open('01_01.data') as file:
    for num in file:
        num = int(num)
        if prev and prev < num:
            ans += 1
        prev = num
print(ans)

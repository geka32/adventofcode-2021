from collections import defaultdict


tmp_board = defaultdict(int)
board = []
instructions = []
with open('13_02.data') as file:
    while line := file.readline().strip():
        x, y = map(int, line.split(','))
        tmp_board[y] |= (1 << x)
    for _ in range(max(tmp_board) + 1):
        board.append(tmp_board[_])
    while line := file.readline().strip():
        along, num = line.split('=')
        instructions.append((along[-1], int(num)))

for along, num in instructions:
    if along == 'y':
        for idx in range(num+1, len(board)):
            board[len(board) - idx - 1] |= board[idx]
        board = board[:num]
    else:
        for idx, row in enumerate(board):
            tmp = row >> (num + 1)
            for idx2 in range(num-1, -1, -1):
                mask = 1 << idx2
                bit = (row & mask) >> idx2
                tmp |= bit << (num - 1 - idx2)
            board[idx] = tmp

for _ in board:
    print(bin(1 << 41 | _)[3:])

"""
01110011110100101111010000111000110010010
01001000010101000001010000100101001010010
01001000100110000010010000100101000011110
01110001000101000100010000111001011010010
01010010000101001000010000100001001010010
01001011110100101111011110100000111010010
 R    Z    K    Z    L    P    G    H
"""
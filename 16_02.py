from typing import List, Optional, Tuple
from dataclasses import dataclass, field
from pprint import pprint


with open('16_02.data') as file:
    BITS = file.readline().strip()

BITS = bin(int(BITS, base=16) | 1 << (len(BITS) * 4))[3:]

"""
8A004A801A8002F478
100 010 1 00000 00000 1 001010100000000001101010000000000000101111010001111000
VVV TTT I LLLLL LLLLL L 001 010 1 00000 00000 1 101010000000000000101111010001111000
                        VVV TTT 1 LLLLL LLLLL L 101 010 0 00000 00000 01011 11010001111000
                                                VVV TTT I LLLLL LLLLL LLLLL 110 100 0 1111 000
                                                                            VVV TTT G AAAA XXX
"""


def read_BITS(offset: int, bits: int) -> Tuple[int, int]:
    return offset + bits, int(BITS[offset:offset+bits], 2)


@dataclass
class Header:
    version: int
    type: int
    offset: int
    tail: int = field(default_factory=int)


@dataclass
class Literal:
    prefix: int = None
    data: int = None

    def read_data(self, offset: int) -> int:
        offset, self.prefix = read_BITS(offset, 1)
        offset, self.data = read_BITS(offset, 4)

        return offset


@dataclass
class Packet:
    header: Header


@dataclass
class PacketLiteral(Packet):
    data: List[Literal] = field(default_factory=list)

    def read_data(self):
        offset = self.header.offset
        while True:
            literal = Literal()
            offset = literal.read_data(offset)
            self.data.append(literal)
            if not literal.prefix:
                break
        self.header.tail = offset

    def get_all_versions_sum(self):
        return self.header.version

    def calculate(self):
        ret = 0
        for _ in self.data:
            ret = (ret << 4) + _.data
        return ret


@dataclass
class PoHeader:
    lenght_id: int
    offset: int
    container_header: Header


@dataclass
class PoHeaderData(PoHeader):
    lenght: int = field(default_factory=int)

    def read_data(self):
        offset = self.offset
        end = offset + self.lenght

        while offset < end:
            packet = read_packet(offset)
            offset = packet.header.tail
            self.container_header.tail = offset

            yield packet


@dataclass
class PoHeaderPackets(PoHeader):
    number: int = field(default_factory=int)

    def read_data(self):
        offset = self.offset

        for _ in range(self.number):
            packet = read_packet(offset)
            offset = packet.header.tail
            self.container_header.tail = offset

            yield packet


@dataclass
class PacketOperator(Packet):
    po_header: PoHeader = None
    data: List[Packet] = field(default_factory=list)

    def read_header(self):
        offset = self.header.offset
        offset, lenght_id = read_BITS(offset, 1)
        if lenght_id == 0:
            offset, lenght = read_BITS(offset, 15)
            self.po_header = PoHeaderData(
                lenght_id, offset, self.header, lenght=lenght)
        else:
            offset, number = read_BITS(offset, 11)
            self.po_header = PoHeaderPackets(
                lenght_id, offset, self.header, number=number)
        self.header.tail = offset

    def read_data(self):
        self.read_header()
        for packet in self.po_header.read_data():
            self.data.append(packet)

    def get_all_versions_sum(self):
        return (self.header.version + sum(_.get_all_versions_sum() for _ in self.data))

    def calculate(self):
        data = [_.calculate() for _ in self.data]
        if self.header.type == 0:
            # print(f"sum of {data}")
            return sum(data)
        elif self.header.type == 1:
            # print(f"product of {data}")
            ret = 1
            for _ in data:
                ret *= _
            return ret
        elif self.header.type == 2:
            # print(f"min of {data}")
            return min(data)
        elif self.header.type == 3:
            # print(f"max of {data}")
            return max(data)
        elif self.header.type == 5:
            # print(f"> of {data}")
            return int(data[0] > data[1])
        elif self.header.type == 6:
            # print(f"< of {data}")
            return int(data[0] < data[1])
        elif self.header.type == 7:
            # print(f"== of {data}")
            return int(data[0] == data[1])
        else:
            assert False, "Unknown operator"


def read_header(offset: int) -> Packet:
    offset, version = read_BITS(offset, 3)
    offset, type = read_BITS(offset, 3)

    if type == 4:
        return PacketLiteral(Header(version, type, offset))
    else:
        return PacketOperator(Header(version, type, offset))


def read_packet(offset: int) -> Packet:
    packet = read_header(offset)
    packet.read_data()

    return packet


packet = read_packet(0)
# pprint(packet)

print(packet.calculate())

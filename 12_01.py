from collections import defaultdict


graph = defaultdict(list)
with open('12_01.data') as file:
    for line in file.readlines():
        a, b = line.strip().split('-')
        graph[a].append(b)
        graph[b].append(a)
# for _ in graph:
#     print(_, ' -> ', graph[_])


def dfs(start_from: str) -> int:
    ans = 0
    for node in graph[start_from]:
        if node == 'end':
            ans += 1
            continue
        elif node.islower() and node in visited:
            continue
        if node.islower():
            visited.add(node)
        ans += dfs(node)
        if node.islower():
            visited.remove(node)
    return ans


visited = {'start'}
print(dfs('start'))

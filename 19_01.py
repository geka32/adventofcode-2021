from itertools import product


scanners = []
with open('19_01.data') as file:
    while starter := file.readline().strip():
        if starter.startswith('---'):
            scanner = []
            while line := file.readline().strip():
                x, y, z = [int(_) for _ in line.split(',')]
                scanner.append([x, y, z])
            scanners.append(scanner)


def rotate(scanner: list, axis: int) -> None:
    """
    Rotate scanners' axis in place.
    """
    for beacon in scanner:
        beacon[axis] = -beacon[axis]


def swap(scanner: list, axis1: int, axis2: int) -> None:
    """
    Swap scanners' axis1 and axis2 in place.
    """
    for beacon in scanner:
        beacon[axis1], beacon[axis2] = beacon[axis2], beacon[axis1]


def get_rotations(scanner: list) -> None:
    """
    Generator. Rotate scanner in place.
    """
    yield
    for axis1, axis2 in ((2, 1), (1, 0)) * 3:
        swap(scanner, axis1, axis2)
        for axis in (0, 1, 2) * 2:
            rotate(scanner, axis)
            yield
        for beacon in scanner:
            beacon[1] = -beacon[1]
        yield
        # for beacon in scanner:
        #     beacon[0], beacon[1], beacon[2] = \
        #         -beacon[0], -beacon[1], -beacon[2],
        # yield scanner


def get_normilized_coordinates(scanner: list) -> list:
    """
    Returns a list of sets of normalizes beacons' coordinates
    get base for every beacon 
    """
    return [{(_[0] - base[0], _[1] - base[1], _[2] - base[2])
             for _ in scanner} for base in scanner]


q = [0]
discovered = {0}
while q:
    idx0 = q.pop()
    scanner_0 = scanners[idx0]
    vals_0 = get_normilized_coordinates(scanner_0)
    for idx1 in range(len(scanners)):
        if idx1 in discovered:
            continue
        # print(f"probe scanners {idx0} and {idx1}")
        scanner_1 = scanners[idx1]
        rotations_1 = get_rotations(scanner_1)

        matches_found = False
        while not matches_found:
            try:
                next(rotations_1)
            except StopIteration as exp:
                break
            vals_1 = get_normilized_coordinates(scanner_1)
            for base_idx0, base_idx1 in product(range(len(vals_0)), range(len(vals_1))):
                matches = vals_0[base_idx0] & vals_1[base_idx1]
                if len(matches) >= 12:
                    matches_found = True
                    # print(f'======== scanner_{idx0} and scanner_{idx1} overlap ========')
                    # scanner_1 is in proper orientation
                    # shift scanner_1 to proper offset
                    a, b = scanner_0[base_idx0], scanner_1[base_idx1]
                    offset = [b[0] - a[0], b[1] - a[1], b[2] - a[2]]
                    for beacon in scanner_1:
                        beacon[0] -= offset[0]
                        beacon[1] -= offset[1]
                        beacon[2] -= offset[2]
                    q.insert(0, idx1)
                    discovered.add(idx1)
                    break

all_beackons = {(beacon[0], beacon[1], beacon[2])
                for scanner in scanners for beacon in scanner}

print(len(all_beackons))

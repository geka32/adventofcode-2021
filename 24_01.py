from __future__ import annotations
from functools import reduce


# requires `pip install z3-solver`
from z3 import Int, Optimize, sat, simplify, is_expr


with open('24_01.data') as file:
    commands = [_.strip().split(maxsplit=1) for _ in file]


class ALU:
    def __init__(self) -> None:
        self.registers = {
            'w': 0,
            'x': 0,
            'y': 0,
            'z': 0,
        }
        self.program: list[Command] = []
        self.command_handlers = {
            'inp': CommandInp,
            'add': CommandAdd,
            'mul': CommandMul,
            'div': CommandDiv,
            'mod': CommandMod,
            'eql': CommandEql,
        }

    def read_commands(self, commands: list[str]) -> None:
        """
            Read program commands
        """
        for command, args in commands:
            self.program.append(
                self.parse_command(command, args)
            )

    def parse_command(self, command: str, args: str) -> Command:
        """
            Parse a command and return executable function
        """
        return self.command_handlers[command](self, args)

    def compile_program(self, input: list[Command]):
        """
            Compile z3 program with provided input.
            Return compiled code in register z.
        """
        self.input = input
        self.input_idx = 0
        for line_num, command in enumerate(self.program):
            if line_num % 18 == 5:
                self.simple_eql = command.register1 > 0
            self.first_eql = line_num % 18 == 6
            command.compile()
        if is_expr(self.registers['z']):
            self.registers['z'] = simplify(self.registers['z'])
        return self.registers['z']


class Command:
    def __init__(self, alu: ALU) -> None:
        self.alu = alu


class CommandTwoOperands(Command):
    def __init__(self, alu: ALU, args: str) -> None:
        super().__init__(alu)
        arg0, arg1 = args.split()
        self.register0 = arg0
        self.register1 = arg1 if arg1 in self.alu.registers else int(arg1)


class CommandInp(Command):
    def __init__(self, alu: ALU, args: str) -> None:
        super().__init__(alu)
        self.register0 = args

    def compile(self) -> None:
        self.alu.registers[self.register0] = self.alu.input[self.alu.input_idx]
        self.alu.input_idx += 1


class CommandAdd(CommandTwoOperands):
    def compile(self) -> None:
        self.alu.registers[self.register0] += self.alu.registers[self.register1] \
            if self.register1 in self.alu.registers else self.register1


class CommandMul(CommandTwoOperands):
    def compile(self) -> None:
        self.alu.registers[self.register0] *= self.alu.registers[self.register1] \
            if self.register1 in self.alu.registers else self.register1


class CommandDiv(CommandTwoOperands):
    def compile(self) -> None:
        self.alu.registers[self.register0] /= self.alu.registers[self.register1] \
            if self.register1 in self.alu.registers else self.register1


class CommandMod(CommandTwoOperands):
    def compile(self) -> None:
        self.alu.registers[self.register0] %= self.alu.registers[self.register1] \
            if self.register1 in self.alu.registers else self.register1


class CommandEql(CommandTwoOperands):
    def compile(self) -> None:
        if self.alu.first_eql:
            return
        elif self.alu.simple_eql:
            self.alu.registers[self.register0] = 1
        else:
            self.alu.registers[self.register0] = \
                self.alu.registers[self.register0] != self.alu.registers['w']


alu = ALU()
alu.read_commands(commands)

o = Optimize()

digits = [Int(f'd{_}') for _ in range(14)]
for digit in digits:
    o.add(digit >= 1, digit < 10)

model_number = Int('m')
o.add(model_number == reduce(lambda a, b: a * 10 + b, digits))

program = alu.compile_program(digits)
o.add(program == 0)

o.maximize(model_number)

assert o.check() == sat, "no solution"

model = o.model()
print(model[model_number])

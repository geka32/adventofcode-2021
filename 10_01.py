chunks = []
with open('10_01.data') as file:
    for line in file.readlines():
        chunks.append(line.strip())

brackets = {
    '{': '}',
    '(': ')',
    '[': ']',
    '<': '>'
}

points = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

ans = 0
for chunk in chunks:
    stack = []
    for char in chunk:
        if char in brackets:
            stack.append(char)
        else:
            open_char = stack.pop()
            if brackets[open_char] != char:
                ans += points[char]
                break

print(ans)

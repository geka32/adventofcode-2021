from collections import defaultdict


graph = defaultdict(list)
with open('12_02.data') as file:
    for line in file.readlines():
        a, b = line.strip().split('-')
        graph[a].append(b)
        graph[b].append(a)
# for _ in graph:
#     print(_, ' -> ', graph[_])


def dfs(start_from: str, double_visited: str) -> int:
    ans = 0
    for node in graph[start_from]:
        if node in ['start', double_visited]:
            continue
        elif node == 'end':
            ans += 1
            continue
        elif node.islower():
            if visited[node]:
                if double_visited:
                    continue
                double_visited = node
                visited[node] += 1
                ans += dfs(node, double_visited)
                visited[node] -= 1
                double_visited = None
            else:
                visited[node] += 1
                ans += dfs(node, double_visited)
                visited[node] -= 1
        else:
            ans += dfs(node, double_visited)
    return ans


visited = defaultdict(int)
print(dfs('start', None))

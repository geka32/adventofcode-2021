input_len = 12
masks = [1 << _ for _ in range(input_len - 1, -1, -1)]

diags = []
with open('03_02.data') as file:
    for diag in file:
        diags.append(int(diag, 2))

oxygen_diags = diags[:]
idx = 0
while len(oxygen_diags) > 1:
    zeros, ones = [], []
    for diag in oxygen_diags:
        if diag & masks[idx]:
            ones.append(diag)
        else:
            zeros.append(diag)
    if len(ones) >= len(zeros):
        oxygen_diags = ones
    else:
        oxygen_diags = zeros
    idx += 1

idx = 0
while len(diags) > 1:
    zeros, ones = [], []
    for diag in diags:
        if diag & masks[idx]:
            ones.append(diag)
        else:
            zeros.append(diag)
    if len(zeros) <= len(ones):
        diags = zeros
    else:
        diags = ones
    idx += 1

print(oxygen_diags[0] * diags[0])

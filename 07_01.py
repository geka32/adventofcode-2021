positions = []
with open('07_01.data') as file:
    for num in file.readline().split(','):
        positions.append(int(num))

positions.sort()
if len(positions) & 1:
    median = positions[(len(positions) + 1) // 2 - 1]
else:
    median = (positions[(len(positions)) // 2 - 1] + positions[(len(positions)) // 2]) // 2

ans = sum(abs(position - median) for position in positions)

print(ans)
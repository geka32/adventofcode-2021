h, d, aim = 0, 0, 0
with open('02_02.data') as file:
    for cmd in file:
        direction, steps = cmd.split()
        if direction.startswith('f'):
            h += int(steps)
            d += aim * int(steps)
        elif direction.startswith('d'):
            aim += int(steps)
        else:
            aim -= int(steps)
print(h * d)

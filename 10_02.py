chunks = []
with open('10_02.data') as file:
    for line in file.readlines():
        chunks.append(line.strip())

brackets = {
    '{': '}',
    '(': ')',
    '[': ']',
    '<': '>'
}

points = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}

chunk_points = []
for chunk in chunks:
    stack = []
    for char in chunk:
        if char in brackets:
            stack.append(char)
        else:
            open_char = stack.pop()
            if brackets[open_char] != char:
                break
    else:
        if stack:
            chunk_scores = 0
            while stack:
                char = stack.pop()
                chunk_scores = chunk_scores * 5 + points[brackets[char]]
            chunk_points.append(chunk_scores)

chunk_points.sort()
print(chunk_points[len(chunk_points) // 2])
